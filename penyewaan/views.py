from .models import Sewa
from django.shortcuts import render
from django.views.generic import DetailView, CreateView
from django.views.generic import UpdateView, DeleteView, View
from django.urls import reverse_lazy
from .utils import Render

# Create your views here.
var = {
    'judul' : 'Sistem Penyewaan Kamar Kost',
    'info' : '''Kami menyediakan penyewaan kamar kost dengan kualitas dan pelayanan yang baik.'''     
}
def index(self):
    var['sewa'] = Sewa.objects.values('id','nama_penyewa','nama_kamar').\
        order_by('nama_penyewa')
    return render(self, 'penyewaan/index.html',context=var)

def sewa(self):
    var['sewa'] = Sewa.objects.values('id','nama_penyewa','nama_kamar').\
        order_by('nama_penyewa')
    return render(self, 'penyewaan/sewa.html',context=var)

class SewaDetailView(DetailView):
    model = Sewa
    template_name = 'penyewaan/sewa_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class SewaCreateView(CreateView):
    model = Sewa
    fields = '__all__'
    template_name = 'penyewaan/sewa_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class SewaEditView(UpdateView):
    model = Sewa
    fields = ['tgl_input','nama_penyewa','nama_kamar',
                'harga']
    template_name = 'penyewaan/sewa_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class SewaDeleteView(DeleteView):
    model = Sewa
    template_name = 'penyewaan/sewa_delete.html'
    success_url = reverse_lazy('sewa')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class SewaToPdf(View):
    def get(self,request):
        var = {
        'sewa' : Sewa.objects.values('nama_penyewa','nama_kamar','harga','tgl_input'),'request':request
        }
        return Render.to_pdf(self,'penyewaan/sewa_to_pdf.html',var)